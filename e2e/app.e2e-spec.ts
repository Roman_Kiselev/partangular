import { PartAngularPage } from './app.po';

describe('part-angular App', () => {
  let page: PartAngularPage;

  beforeEach(() => {
    page = new PartAngularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
