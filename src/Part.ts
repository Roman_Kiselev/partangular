export class Part {
  id: number;
  partName: string;
  partNumber: string;
  vendor: string;
  quantity: number;
  shipped: Date;
  received: Date;
}
