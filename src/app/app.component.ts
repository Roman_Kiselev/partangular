import { Component, Input } from '@angular/core';
import {OrderDirection, PartFilter} from '../PartFilter';
import {Part} from '../Part';
import {Http, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import {Headers} from '@angular/http'
import {isNumeric} from 'rxjs/util/isNumeric';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private url = 'http://localhost:9081/api/parts';
  headers = new Headers({ 'Content-Type': 'application/json' });
  options = new RequestOptions({ headers: this.headers });
  parts: Part[];
  @Input() afterReceived: string;
  @Input() beforeReceived: string;
  @Input() afterShipped: string;
  @Input() beforeShipped: string;
  afterReceivedError: boolean = false;
  beforeReceivedError: boolean = false;
  afterShippedError: boolean = false;
  beforeShippedError: boolean = false;
  quantityError: boolean = false;

  @Input() filter: PartFilter = new PartFilter;
  constructor(private http: Http) {}
  search() {
    if (this.afterReceived) {
      this.filter.afterReceived = new Date(Date.parse(this.afterReceived));
      this.afterReceivedError = isNaN(this.filter.afterReceived.getDate());
    }else{
      this.afterReceivedError = false;
      this.filter.afterReceived = null;
    }
    if (this.beforeReceived) {
      this.filter.beforeReceived = new Date(Date.parse(this.beforeReceived));
      this.beforeReceivedError = isNaN(this.filter.beforeReceived.getDate());
    }else{
      this.beforeReceivedError = false;
      this.filter.beforeReceived = null;
    }
    if (this.afterShipped) {
      this.filter.afterShipped = new Date(Date.parse(this.afterShipped));
      this.afterShippedError = isNaN(this.filter.afterShipped.getDate());
    }else{
      this.afterShippedError = false;
      this.filter.afterShipped= null;
    }
    if (this.beforeShipped) {
      this.filter.beforeShipped = new Date(Date.parse(this.beforeShipped));
      this.beforeShippedError = isNaN(this.filter.beforeShipped.getDate());
    }else{
      this.beforeShippedError = false;
      this.filter.beforeShipped = null;
    }
    if (this.filter.quantity){
      this.quantityError = !isNumeric(this.filter.quantity);
    }else{
      this.quantityError = false;
    }
    if (!this.afterReceivedError && !this.beforeReceivedError && !this.afterShippedError && !this.beforeShippedError && !this.quantityError) {
      this.http.post(this.url, JSON.stringify(this.filter), this.options).toPromise().then(response => {
        this.parts = (response.json() as Part[]);
        return response.json() as Part[];
      });
    }
  }
  order(field: string) {
    if (this.filter.orderField === field) {
      if (this.filter.direction === OrderDirection.ASC) {
        this.filter.direction = OrderDirection.DESC;
      }else {
        this.filter.direction = OrderDirection.ASC;
      }
    }else {
      this.filter.orderField = field;
      this.filter.direction = OrderDirection.ASC;
    }
    this.search();
  }
}
