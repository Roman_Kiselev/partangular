export class PartFilter {
  partName: string;
  partNumber: string;
  vendor: string;
  quantity: number;
  afterShipped: Date;
  beforeShipped: Date;
  afterReceived: Date;
  beforeReceived: Date;
  orderField: string;
  direction: OrderDirection;
}
export enum OrderDirection {
  ASC,
  DESC
}

