System requirements:
1. NodeJs, https://nodejs.org/en/download/
   NodeJs comes with npm installed, but it's necessary to have the latest version, perform the command 'npm install npm@latest -g' in command line.
2. ng-cli
   Perform in command line 'npm install -g ng-cli'
3. tomcat, http://tomcat.apache.org/download-80.cgi
   
 For deploy it's necessary to perform the command in command line: 'ng build -prod'. After that, the directory 'PartAngular' 
 will  be generated. This directory needed copy into tomcat's webapp folder (%TOMCAT_HOME%)/webapps/). 
 First, the backend must be launched, after that - navigate to  http://localhost:8080/PartAngular

